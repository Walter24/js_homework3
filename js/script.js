// Рішення за допомогою циклу for:
// function factorial(n) {
    
//     let res = 1;
//     for (let i = 1; i <= n; i++) {
//         res = res * i;
//     }
//     return res; 
// }
// let n = +prompt("Введіть ціле число", "1");
// alert( factorial(n) );


// Рішення за допомогою циклу while та декременту:
function factorial(n) {
    
    let res = 1;
    let i = n;
    while (i) {
        res *= i;
        i--
    }
    return res; 
}
let n = +prompt("Введіть ціле число", "1");
alert( factorial(n) );



// Рішення через рекурсію:
// function factorial(n) {
//     if (n > 1) {                  // можна було викоритати класичний вираз для рекурсії if (n != 1)
//         return n * factorial(n-1);
//     } else {
//         return 1;
//     }
// }
// let n = +prompt("Введіть ціле число", "1");
// alert( factorial(n) );